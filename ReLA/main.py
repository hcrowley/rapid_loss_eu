#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 16:48:34 2022

@author: helencrowley

This script launches a loss/damage calculation for any event in the European ShakeMap system

"""


#%%
import requests
import os
import run_risk_calculation as run_job
import prepare_exposure as prep_exp
import geopandas as gpd


#%%
        
def main (exp_shape):
                    
    print ('Download European ShakeMap for event id:',(unid))
    if not os.path.exists('../inputs'):
        os.mkdir('../inputs')
    if not os.path.exists('../outputs'):
        os.mkdir('../outputs') 
    if not os.path.exists('../inputs/{}'.format(unid)):
        os.mkdir('../inputs/{}'.format(unid))
    if not os.path.exists('../outputs/{}'.format(unid)):
        os.mkdir('../outputs/{}'.format(unid))    
    grid = requests.get('http://shakemapeu.ingv.it/data/{}/current/products/grid.xml'.format(unid)).text
    uncertainty = requests.get('http://shakemapeu.ingv.it/data/{}/current/products/uncertainty.xml'.format(unid)).text
    with open(os.path.join('../inputs',unid,'grid_{}.xml'.format(unid)), "w") as f:
        f.write(grid) 
    with open(os.path.join('../inputs',unid,'uncertainty_{}.xml').format(unid), "w") as f:
        f.write(uncertainty)

    # download exposure data and get country list

    countries_with_exposure = prep_exp.prep_exposure(unid, exp_shape)       

    #run risk calculations (unless exposure is empty)     
        
    if len(countries_with_exposure) != 0:

        run_job.run_loss(unid, countries_with_exposure)
        with open('../inputs/log.txt', 'a') as f:
            f.write(f'-----------------{unid}------------------\n Analyses completed, check the log files for any OQ errors\n')
            f.flush()                   
    else:
        
        print('This event does not impact any buildings')
        with open('../outputs/{}/log_out.txt'.format(unid), 'a') as f:
            f.write(f'-----------------{unid}------------------\n This event does not impact any buildings\n')
            f.flush()         
         
    os.system('oq reset -y') # remove the oq database 
    os.system('oq engine --duc')
   

#%% Run main function if called as script

if __name__ == "__main__":
     
    unid = input('Unique ID of the event from the European ShakeMap system: ') 

    # download and parse the ShakeMap system to get details of the event
    url = 'http://shakemapeu.ingv.it/events.json'
    html_content = requests.get(url).text
    eventdata = html_content.split("},")

    exp_shape = gpd.read_file('../shapefile/Europe.shp') 
   
    main(exp_shape) 

