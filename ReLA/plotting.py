#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 18:22:52 2022

@author: sevgi
modified: hcrowley

"""
import numpy as np
import matplotlib.pyplot as plt
from numpy import random
from openquake.commonlib.datastore import read


def loss_plot(unid, country):
        
    dstore = read(-1) #last calculation
    data = dstore.read_df('risk_by_event', 'event_id')
  
    #%%
    #----fatalities_histogram----
    
    fatalities=data[data.loss_id == 4] #this is 0 in OQ versions before v3.15
    
    # add variability to account for uncertainty in vulnerability model
    log_sd = 1.5
    log_mean = np.log(fatalities.loss)-0.5*log_sd**2
    fatalities = []
    for i in range(0,100):
        n = random.lognormal(mean=log_mean, sigma=log_sd, size=None)
        fatalities.append(n)
        
    fatalities =np.concatenate(fatalities)
                
    hist, bins = np.histogram(fatalities, bins=[0,1, 10, 100, 1000, 10000,100000])
    if hist.sum() > 0:
        totfatalities_mean = round(fatalities.mean(),1)
        totfatalities_median = np.exp(np.log(totfatalities_mean)-0.5*log_sd**2)
        perc=np.round(hist/hist.sum()*100,1)
    
        colors = ['GREEN', 'YELLOW', 'YELLOW','ORANGE', 'RED', 'RED', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        
        if totfatalities_median > 1000:
            fatality_color = 'RED'
        elif totfatalities_median > 100:
            fatality_color = ' ORANGE'
        elif totfatalities_median > 1:
            fatality_color = ' YELLOW'
        else:
            fatality_color = ' GREEN'
        #fatality_color = colors[np.argmax(hist)]

        n, bins, patches=plt.hist(fatalities, bins)
        
        # adapt the color of each patch
        for c, p in zip(colors, patches):
            p.set_facecolor(c)
        
        plt.xscale('log')
        current_values = plt.gca().get_xticks()
        plt.gca().set_xticklabels(['{:.0f}'.format(x) for x in current_values])
        plt.gca().set_yticklabels([])
        plt.xlim([0.1,100000])
        rects = plt.gca().patches
        labels = [i for i in perc]
        
        
        for rect, label in zip(rects, labels):
            height = rect.get_height()
            plt.gca().text(rect.get_x() + rect.get_width() / 2, height+0.01, str(label)+'%',
                            ha='center', va='bottom')
        plt.xlabel('Fatalities',fontsize=10)
        plt.savefig('../outputs/{}/{}_Fatalities_{}.jpeg'.format(unid,country,unid), dpi=250)
        plt.clf()
        plt.close()
    else:
        fatality_color = 'GREEN'
        totfatalities_mean = 0
        totfatalities_median = 0
       
        
    #%%
    #----economic losses_histogram----     
    
    # add the uncertainty due to vulnerability uncertainty
    loss=data[data.loss_id == 3] #this is 1 in OQ versions before v3.15
    #variance = loss.variance
    #sd = variance**0.5
    #log_sd = (np.log((sd/loss.loss)**2+1))**0.5
    log_sd = 1.5
    log_mean = np.log(loss.loss)-0.5*log_sd**2
    loss = []
    for i in range(0,100):
        n = random.lognormal(mean=log_mean, sigma=log_sd, size=None)
        loss.append(n)
            
    loss =np.concatenate(loss)
    
    hist, bins = np.histogram(loss/1000000, bins=[0,1, 10, 100, 1000, 10000,100000])
    if hist.sum() > 0:
        totloss_mean = round(loss.mean()/1000000,2)
        totloss_median = np.exp(np.log(totloss_mean)-0.5*log_sd**2)
        perc=np.round(hist/hist.sum()*100,1)
        colors = ['GREEN', 'YELLOW', 'YELLOW','ORANGE', 'RED', 'RED', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        
        if totloss_median > 1000:
            loss_color = 'RED'
        elif totloss_median > 100:
            loss_color = ' ORANGE'
        elif totloss_median > 1:
            loss_color = ' YELLOW'
        else:
            loss_color = ' GREEN'
        #colors[np.argmax(hist)]

        n, bins, patches=plt.hist(loss/1000000, bins)
        
        # adapt the color of each patch
        for c, p in zip(colors, patches):
            p.set_facecolor(c)
        
        plt.xscale('log')
        current_values = plt.gca().get_xticks()
        plt.gca().set_xticklabels(['{:.0f}'.format(x) for x in current_values])
        plt.gca().set_yticklabels([])
        plt.xlim([0.1,100000])
        rects = plt.gca().patches
        labels = [i for i in perc]
        
        
        for rect, label in zip(rects, labels):
            height = rect.get_height()
            plt.gca().text(rect.get_x() + rect.get_width() / 2, height+0.01, str(label)+'%',
                            ha='center', va='bottom')
        plt.xlabel('Economic_loss (millions)',fontsize=10)
        plt.savefig('../outputs/{}/{}_Economic_losses_{}.jpeg'.format(unid,country,unid), dpi=250)
        plt.clf()
        plt.close()  
     
    else:
        loss_color = 'GREEN'
        totloss_mean = 0
        totloss_median = 0
             
    return fatality_color, loss_color, totloss_median, totfatalities_median
    
def damage_plot(unid, country):
        
    dstore = read(-1) #last calculation
    data2 = dstore.read_df('risk_by_event', 'event_id')    
        
    #----complete_damaged_histogram----
    
    damage=data2    
    hist, bins = np.histogram(damage['dmg_4'], bins=[0,1, 10, 100, 1000, 10000,100000])
       
    if hist.sum() > 0:
        #totcomdamage = round(damage['dmg_4'].mean(),1)
        totcomdamage_mean = int(damage['dmg_4'].mean())
        totcomdamage_median = int(damage['dmg_4'].median())

        perc=np.round(hist/hist.sum()*100,1)
        colors = ['GREEN', 'YELLOW', 'YELLOW','ORANGE', 'RED', 'RED', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        
        if totcomdamage_median > 1000:
            damage_color = 'RED'
        elif totcomdamage_median > 100:
            damage_color = ' ORANGE'
        elif totcomdamage_median > 1:
            damage_color = ' YELLOW'
        else:
            damage_color = ' GREEN'

        #damage_color = colors[np.argmax(hist)]
 
        n, bins, patches=plt.hist(damage['dmg_4'], bins)
        
        # adapt the color of each patch
        for c, p in zip(colors, patches):
            p.set_facecolor(c)
        
        plt.xscale('log')
        current_values = plt.gca().get_xticks()
        plt.gca().set_xticklabels(['{:.0f}'.format(x) for x in current_values])
        plt.gca().set_yticklabels([])
        plt.xlim([0.1,100000])
        rects = plt.gca().patches
        labels = [i for i in perc]
        
        
        for rect, label in zip(rects, labels):
            height = rect.get_height()
            plt.gca().text(rect.get_x() + rect.get_width() / 2, height+0.01, str(label)+'%',
                            ha='center', va='bottom')
        plt.xlabel('Completely Damaged Buildings',fontsize=10)
        plt.savefig('../outputs/{}/{}_Complete_Damage_{}.jpeg'.format(unid,country,unid), dpi=250)
        plt.clf()
        plt.close()
    else:
        damage_color = 'GREEN'
        totcomdamage = 0
        
    return damage_color,totcomdamage_median