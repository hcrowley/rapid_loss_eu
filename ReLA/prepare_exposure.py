#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 12:35:37 2022

@author: helencrowley

This script cuts the three exposure CSV files that make up the input for 
running damage or loss calculations on OpenQuake, according to the extent of
the ShakeMap.


"""

import geopandas as gpd
from shapely.geometry import Polygon
import xml.etree.ElementTree as ET
import os
from io import StringIO 
from shapely import geometry
import pandas as pd
from utils import get_filenames
from utils import get_tags
from write_to_xml import exposure_to_xml
import gc

#%%

def prep_exposure(unid,exp_shape):        

    # reduce ShakeMap to only cover MMI >= 3 
    
    min_MMI=3
    grid_file=os.path.join('../inputs/',unid,'grid_{}.xml'.format(unid))
    
    with open(grid_file):
        ET.register_namespace("","http://earthquake.usgs.gov/eqcenter/shakemap")
        ET.register_namespace("xsi","http://www.w3.org/2001/XMLSchema-instance")
        ET.register_namespace("xsi:schemaLocation","http://earthquake.usgs.gov http://earthquake.usgs.gov/eqcenter/shakemap/xml/schemas/shakemap.xsd")
    
        tree = ET.parse(grid_file)
        doc = tree.getroot()
        
        grid_data = doc.find('.//{http://earthquake.usgs.gov/eqcenter/shakemap}grid_data').text
        df = pd.read_table(StringIO(grid_data), sep="\\s+", header=None,
                             names=['LON','LAT','MMI','PGA', 'PGV','PSA03','PSA10','PSA30','SVEL'])
    
        df2 = df[df.MMI>min_MMI]
        
        if len(df2)>0: 
            new_grid_data=df2.to_string(index=False,header=False)
        
            elem = doc.findall('.//{http://earthquake.usgs.gov/eqcenter/shakemap}grid_data')[0]
            elem.text=new_grid_data
        
            doc[1].attrib['lon_min']= str(min(df2.LON))
            doc[1].attrib['lat_min']= str(min(df2.LAT))
            doc[1].attrib['lon_max']= str(max(df2.LON))
            doc[1].attrib['lat_max']= str(max(df2.LAT))
            doc[1].attrib['nlat']= str(len(df2.LAT.unique()))
            doc[1].attrib['nlon']= str(len(df2.LON.unique()))
            tree.write(os.path.join('../inputs',unid,'grid_red_{}.xml'.format(unid)), xml_declaration=True, method='xml', encoding="UTF-8")
    
            # read the extent of the reduced ShakeMap
            
            tree = ET.parse(os.path.join('../inputs',unid,'grid_red_{}.xml'.format(unid)))
            root = tree.getroot()
            sm_grid = root[1].attrib
            
            # create a polygon of extent
            
            p1 = geometry.Point(float(sm_grid['lon_min']),float(sm_grid['lat_min']))
            p2 = geometry.Point(float(sm_grid['lon_max']),float(sm_grid['lat_min']))
            p3 = geometry.Point(float(sm_grid['lon_max']),float(sm_grid['lat_max']))
            p4 = geometry.Point(float(sm_grid['lon_min']),float(sm_grid['lat_max']))
            
            pointList = [p1, p2, p3, p4, p1]
            
            polygon_geom = Polygon(([[p.x, p.y] for p in pointList]))
            crs = ('epsg:4326')
            polygon = gpd.GeoDataFrame(index=[0], crs=crs, geometry=[polygon_geom]) 
            
            # obtain min and max lon and lat from the extent
            
            lon_min = float(sm_grid['lon_min'])
            lon_max = float(sm_grid['lon_max'])
            lat_min = float(sm_grid['lat_min'])
            lat_max = float(sm_grid['lat_max'])
            
            # join the shakemap extent with the res shapefile to find countries of interest
            
            join_sm_exp_res = gpd.sjoin(polygon, exp_shape, predicate='intersects')
            del exp_shape
            gc.collect()
            
            
            countries_with_exposure=[]
            for country in join_sm_exp_res.Name.unique():
                if country != None:
                    print('Country affected: {}'.format(country))
               
                    occupancies = ["Ind", "Com", "Res"]
                   
                    for occu in occupancies:
                        print("Processing exposure models for occupancy: %s" %(occu))             
                        path_in = '../../esrm20/Exposure_30arcsec'
                        filename = os.path.join(path_in,'OQ_Exposure_30arcsec_Input_{}_{}.csv'.format(country, occu))
                        in_df = pd.read_csv(filename)
                        out_df = in_df[(in_df["lon"] >= lon_min) & (in_df["lon"] <= lon_max) & (in_df["lat"] >= lat_min) & (in_df["lat"] <= lat_max)]                
                        path_out = os.path.join('../inputs',unid)
                        if len(out_df) != 0:
                            filename_out = 'OQ_Exposure_30arcsec_Input_{}_{}.csv'.format(country, occu)                    
                            out_df.to_csv(os.path.join(path_out, filename_out), index=False)
                                            
                    tags = ['occupancy']
                    csv_files = []
                    exposure_files =  get_filenames(path_out,contain='OQ_Exposure_30arcsec_Input_{}_....csv'.format(country))
                    
                    for row in exposure_files.itertuples():
                        file_path =  os.path.join(row.Path, row.File_Name)
                        df = pd.read_csv(file_path)
                        df.columns = df.columns.str.lower()
                        tags = get_tags(list(df.columns), tags)
                        csv_files.append(row.File_Name)
                    if csv_files:
                        save_xml = os.path.join(path_out, 'OQ_Exposure_30arcsec_Input_%s.xml' % country)
                        exposure_to_xml(csv_files, tags, save_xml)
                        countries_with_exposure.append(country) 
            
        else:
            countries_with_exposure=[]
   
    
    return countries_with_exposure
            
        





       
        
        