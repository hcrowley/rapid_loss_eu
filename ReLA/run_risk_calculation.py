#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 19:37:40 2022

@author: helencrowley

Prepares job files and runs job with OQ engine

"""

import os
import shutil
import subprocess as sp
import plotting as plot
import pytz
from datetime import datetime
import pandas as pd
from bs4 import BeautifulSoup 

def run_loss(unid, country_list):
    
    flag = -1 
    for country in country_list:  
        flag = flag+1
                
        # extract time from the grid xml
        
        grid_file=os.path.join('../inputs/',unid,'grid_{}.xml'.format(unid))
        
        with open(grid_file, encoding="utf8") as f:
            data = f.read()
            bsoup=BeautifulSoup(data, "xml")
            hrefs =  bsoup.find_all("event")
        event_time = []
        for href in hrefs:
            links = href.get("event_timestamp")
            event_time.append(links)
        
         # get local time of event in country
        
        timezones = pd.read_csv('../shapefile/Time_Zones.csv')
        local_timezone = timezones[timezones.Country==country].Timezone.iloc[0]
        
        time =datetime.strptime(event_time[0][:19],'%Y-%m-%dT%H:%M:%S')
        utc_time = datetime.fromtimestamp(time.timestamp()).replace(tzinfo=pytz.UTC)
        local_time = utc_time.astimezone(pytz.timezone(local_timezone))
        time_event = local_time.hour
        
         # get time of day
        
        if 10 <= time_event < 18:
            time_day = 'day'
        elif 6 <= time_event < 10:
            time_day = 'transit'
        elif 18 <= time_event < 22:
            time_day = 'transit'
        else:
            time_day = 'night'      
               
        # prep file for losses
            
        template = '../templates/job_shakemap_prep_losses_template.ini'
        job_temp = open(template, 'r')
        shutil.copyfile(template, os.path.join('../inputs',unid,'job_shakemap_prep_losses_{}_{}.ini'.format(str(unid), country)))
        job = open(os.path.join('../inputs',unid,'job_shakemap_prep_losses_{}_{}.ini'.format(str(unid), country)), 'w')
        
        for line in job_temp.readlines():
                if 'unid' in line:
                    line = line.replace('unid', unid)
                if 'period' in line:
                    line = line.replace('period', time_day)
                job.write(line)
                
                
        job = open(os.path.join('../inputs',unid,'job_shakemap_prep_losses_{}_{}.ini'.format(str(unid), country)), 'a')
        
            
        job.write( ' OQ_Exposure_30arcsec_Input_{}.xml'.format(country))
        job.write("\n")
                  
        job.close()
        
        
        # prep file for damage
            
        template = '../templates/job_shakemap_prep_damage_template.ini'
        job_temp = open(template, 'r')
        shutil.copyfile(template, os.path.join('../inputs',unid,'job_shakemap_prep_damage_{}_{}.ini'.format(str(unid), country)))
        job = open(os.path.join('../inputs',unid,'job_shakemap_prep_damage_{}_{}.ini'.format(str(unid), country)), 'w')
        
        for line in job_temp.readlines():
                if 'unid' in line:
                    line = line.replace('unid', unid)
                job.write(line)
                
        job = open(os.path.join('../inputs',unid,'job_shakemap_prep_damage_{}_{}.ini'.format(str(unid), country)), 'a')
    
            
        job.write( ' OQ_Exposure_30arcsec_Input_{}.xml'.format(country))
        job.write("\n")
                  
        job.close()

        # run file for losses
        
        template = '../templates/job_shakemap_run_losses_template.ini'
        job_temp = open(template, 'r')
        shutil.copyfile(template, os.path.join('../inputs',unid,'job_shakemap_run_losses_{}_{}.ini'.format(str(unid), country)))
        job = open(os.path.join('../inputs',unid,'job_shakemap_run_losses_{}_{}.ini'.format(str(unid), country)), 'w')
        
        for line in job_temp.readlines():
                if 'unid' in line:
                    line = line.replace('unid', unid)
                job.write(line)
                
        job.close()
        
        
        # run calculations - losses and plot    
         
        print('Preparing loss calculation for {} in {}'.format(unid, country))
        os.system('oq engine --log-file ../inputs/{}/log_prep_losses_{}.txt --run ../inputs/{}/job_shakemap_prep_losses_{}_{}.ini'.format(unid, country, unid,unid, country))  
        
               
        calc_outputs = sp.getoutput('oq engine --lhc')
        hc = calc_outputs.splitlines()[-1][0:6].strip() 
        
        print('Running loss calculation for {} in {}'.format(unid, country))            
        os.system('oq engine --log-file ../inputs/{}/log_run_losses_{}.txt --run ../inputs/{}/job_shakemap_run_losses_{}_{}.ini --hc {}'.format(unid,country,unid,unid,country,hc))
        
        
        calc_outputs = sp.getoutput('oq engine --lrc')
        rc1 = calc_outputs.splitlines()[-1][0:6].strip()
        
        print ('OQ risk calculation number {} complete'.format(rc1))

        [fatality_color, loss_color, totlosses_median, totfatalities_median] = plot.loss_plot(unid, country)
        
        # run damage file 
        
        template = '../templates/job_shakemap_run_damage_template.ini'
        job_temp = open(template, 'r')
        shutil.copyfile(template, os.path.join('../inputs',unid,'job_shakemap_run_damage_{}_{}.ini'.format(str(unid), country)))
        job = open(os.path.join('../inputs',unid,'job_shakemap_run_damage_{}_{}.ini'.format(str(unid), country)), 'w')
        
        for line in job_temp.readlines():
                if 'unid' in line:
                    line = line.replace('unid', unid)
                job.write(line)
                
        job.close()
        
        
        # run calculations - fragility and plot
        
        print('Preparing damage calculation for {} in {}'.format(unid, country))
        os.system('oq engine --log-file ../inputs/{}/log_prep_damage_{}.txt --run ../inputs/{}/job_shakemap_prep_damage_{}_{}.ini'.format(unid,country,unid,unid, country))  
        
        calc_outputs = sp.getoutput('oq engine --lhc')
        hc = calc_outputs.splitlines()[-1][0:6].strip()
                    
        print('Running damage calculation for {} in {}'.format(unid, country))         
        os.system('oq engine --log-file ../inputs/{}/log_run_damage_{}.txt --run ../inputs/{}/job_shakemap_run_damage_{}_{}.ini --hc {}'.format(unid,country,unid,unid,country,hc))
             
        calc_outputs = sp.getoutput('oq engine --lrc')
        rc2 = calc_outputs.splitlines()[-1][0:6].strip() 
        
        print ('OQ risk calculation number {} complete'.format(rc2))

        [damage_color,totcomdamage_median] = plot.damage_plot(unid, country)

        # print outputs        

        if flag == 0:
            with open('../outputs/{}/log_out.txt'.format(unid), 'a') as f:
                f.write(f'NOTE! This is a test service and the data below have NOT been reviewed by a scientist.\n{unid}, {country}, {loss_color}, Median economic loss (M EUR): {totlosses_median}\n{unid}, {country}, {fatality_color}, Median fatalities: {totfatalities_median}\n{unid}, {country}, {damage_color}, Median number of completely damaged buildings: {totcomdamage_median}\n')
                f.flush() 
        else:
            with open('../outputs/{}/log_out.txt'.format(unid), 'a') as f:
                f.write(f'{unid}, {country}, {loss_color}, Median economic loss (M EUR): {totlosses_median}\n{unid}, {country}, {fatality_color}, Median fatalities: {totfatalities_median}\n{unid}, {country}, {damage_color}, Median number of completely damaged buildings: {totcomdamage_median}\n\n')
                f.flush()   
        

        # download outputs 
                     
        #os.system('oq engine --eos {} ../outputs_oq/{}'.format(rc1,unid))
        #os.system('oq engine --eos {} ../outputs_oq/{}'.format(rc2,unid))
        
     
        
    