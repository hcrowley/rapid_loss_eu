# European Rapid earthquake Loss Assessment (ReLA) Code

## Overview 

The purpose of this code is to allow users to set up a Rapid earthquake Loss Assessment (ReLA) service that can be used in any country in Europe. The software reads a user-defined ShakeMap from the [European ShakeMap service](http://shakemapeu.ingv.it/), launches the OpenQuake engine to run loss and damage calculations, downloads and stores results and plots locally. 

## Installation

First download or clone this repository.

Install the OpenQuake engine (v3.15) using the instructions [here](https://github.com/gem/oq-engine/blob/engine-3.15/doc/installing/universal.md). 

Activate the OpenQuake virtual environment (as described in the instructions above), then navigate to the main working directory of this repository and install all dependencies using ``pip``, by running the following command:

```
    pip install -r requirements.txt
```

The dependencies are as follows:

* geopandas
* pyogrio
* pygeos
* lxml
* bs4

## Getting started

The software uses models from the esrm20 repository, available [here](https://gitlab.seismo.ethz.ch/efehr/esrm20).  The software will expect to find a folder called `esrm20` in the same directory as this repository with the following structure:
- `Exposure_30arcsec`: Directory into which you should copy the exposure `.csv` files from the esrm20 `Exposure_30arcsec` repository, for all the countries of interest.
- `Vulnerability`: Directory into which you should copy all of the xml files from the esrm20 `Vulnerability` repository with 'ShakeMap' in the name, as well as the 'esrm20_exposure_vulnerability_mapping.csv' file. 

From the command line (terminal) navigate to the ReLA folder and run the software with:

```
python main.py
```

You will be prompted to input the event id of a ShakeMap. Any event id from the [European ShakeMap archive](http://shakemapeu.ingv.it/archive.html) can be input. 

Once the analyses have completed you will find three new folders on the repository: 
- `inputs`: this includes a directory with the name of the ShakeMap unique ID, inside which you will find the downloaded ShakeMap grid and uncertainty files, a reduced ShakeMap (with intensity values greater than or equal to III), the 30 arc second exposure models that have been cut to the size of the reduced ShakeMap, as well as the log files of the OpenQuake engine calculations (all of these are useful for results checking and debugging).
- `outputs`: this includes a directory with the name of the ShakeMap unique ID, inside which you will find a summary of the mean losses/damage as well as loss/damage distribution plots.
- `outputs_oq`: this includes a directory with the name of the ShakeMap unique ID, inside which you will find all of the OpenQuake engine outputs for the calculation (this provides access to additional outputs, and can be used for further checking and debugging). 


## Acknowledgements

These tools have been developed within the [RISE project](http://rise-eu.org/home/), which has
received funding from the European Union's Horizon 2020 research and innovation programme under
grant agreement No. 821115.


## License

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source. For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code. There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

See the [LICENSE](./LICENSE) for the full license text.
